#pragma once

namespace ff12
{
	class VBFReaderException : public std::runtime_error
	{
	public:
		VBFReaderException(const std::string& msg)
			: std::runtime_error(msg)
		{
		}
		VBFReaderException()
			: std::runtime_error("Unknown error")
		{
		}
		~VBFReaderException() = default;
	};

	class VBFReader
	{
	public:
		struct unpackedFile
		{
			std::unique_ptr<uint8_t[]> data;
			uint64_t size;

			// Check whether access will be valid
			bool validateRange(uint64_t offset, uint64_t length) const
			{
				if (!data) return false;
				if (offset >= size) return false;
				if (length > size) return false;
				if ((size - offset) < length) return false;
				return true;
			}
		};
		VBFReader() = default;
		VBFReader(const std::wstring& path);
		bool open(const std::wstring& path);
		void close();
		//bool unpackSave(const std::string& inFile, const std::filesystem::path& outFile);
		unpackedFile unpack(const std::string& inFile, uint32_t blocks = 0);
		bool fileExists(const std::string& inFile);
		~VBFReader();



		operator bool() const;
	private:
		struct VBF_HEADER
		{
			uint32_t magic;
			uint32_t headerLength;
			uint64_t numOfFiles;
		};

		struct VBF_FILE
		{
			uint32_t blocklistStart;
			uint32_t unk1; //same as blocklistStart
			uint64_t originalSize;
			uint64_t startOffset;
			uint64_t filenameOffset;
		};

		struct VBF_ENTRY
		{
			uint64_t originalSize;
			uint64_t startOffset;
			std::vector<uint16_t> blocklist;
		};
		uint32_t _getBlockCount(const std::vector<VBF_FILE>& vbfIndex);
		void _populateEntries(const std::vector<VBF_FILE>& vbfIndex, const char* stringTable, const std::vector<uint16_t>& vbfBlockIndex);
		void _reset();
		static std::string _getNormalizedPath(const std::string& inFile);
		std::ifstream _file;
		std::filesystem::path _filename;
		std::map<std::string, VBF_ENTRY> _vbfEntries;
		static constexpr uint32_t _max_block_size = 64*1024;
	};

}
