#pragma once
#include <filesystem>
#include <vector>
#include "VBFReader.h"

namespace ff12
{
	//phyre
#pragma pack(8) //just in case anything changes
	struct streamerStruct24
	{
		void* blockBuffer;
		uint64_t unk2;
		uint64_t unk3;
	};
	struct streamStructMain
	{
		uint64_t unk1;
		FILE* file;
		uint64_t unk2;
		uint64_t unk3;
		uint64_t totalBytesRead;
		streamerStruct24* streamer24;
	};
	struct readInfoStruct
	{
		uint32_t bytesToRead;
		HANDLE hFile;
		streamStructMain* stream;
		uint8_t unk3;
		uint64_t unk4;
		uint32_t fileSize;
	};
#pragma pack()

	extern "C" streamStructMain * jumpToOpen(void* hProv, char* filename);

	class FileLoader
	{
	public:
		static bool testPatchesHooks();
		static bool initPatchesHooks();
		static void setModulePath(const std::filesystem::path& path);
		static void readConfig();
		static void setModuleBase(uintptr_t address);

	private:
		//original functions to call in hooks
		//open phyre
		using openPhyreType = streamStructMain * (*)(void* hProv, char* filename);

		//cleaning up outside of destructors
		using destroyMusicType = void(*)(uintptr_t* objectThis);

		//ps2 size
		using getSizeType = int(*)(int section, int index);
		using getFileNameType = const char* (*)(int section, int index);

		using readVBFByIdType = int (*)(int section, int index, void *dest, unsigned int size, int startOffset);
		

		//addresses of calls to hook
		static constexpr intptr_t LOADFSTCALL = 0x22A159;
		static constexpr intptr_t OPENPHYRE = 0x59F192;
		static constexpr intptr_t DESTROYFREEMOVIE1 = 0x6CCA9C;
		static constexpr intptr_t DESTROYFREEMOVIE2 = 0x1D6E96;
		static constexpr intptr_t DESTROYFREEMUSIC = 0x5982C9;
		static constexpr intptr_t GETSIZE1 = 0x46DE70;
		static constexpr intptr_t GETSIZE2 = 0x40A15A;
		static constexpr intptr_t READVBFBYID = 0x46E03E;

		//patch addresses
		static constexpr intptr_t EMPTYSTREAM = 0x59F19E;
		static constexpr intptr_t DESTROYREPLACE = 0x598100;
		static constexpr intptr_t CHECKEXISTSPATCH = 0x2DF7B9;

		//hook functions
		static streamStructMain* openPhyreHook(void* hProv, char* filename, readInfoStruct* readInfo);
		static void destroyReader(uint64_t* objectThis);
		static void destroyReaderMovieHook(readInfoStruct* readInfo);
		static void destroyReaderMusicHook(uintptr_t* objectThis);
		static int getSizeHook(int section, int index);
		static int readVBFByIdHook(int section, int index, void* dest, unsigned int size, int startOffset);

		//helper functions
		
		static VBFReader::unpackedFile getBaseFile(const std::filesystem::path& file, std::vector<std::filesystem::path>::const_iterator it, const std::vector<std::filesystem::path>::const_iterator cend, uint32_t blocks = 0);
		
		static constexpr uintptr_t _defaultModuleBase = 0x120000;
		static uintptr_t _moduleBase;
		static uintptr_t getAddressASLR(uintptr_t address);

		struct tHookInit
		{
			const char* name;
			const intptr_t callPtr;
			const intptr_t hookPtr;
			const uint8_t original[5];
		};
		static const std::vector<tHookInit> _allHooks;

		struct tPatchBytes
		{
			const char* name;
			const intptr_t addr;
			const std::vector<uint8_t> patch;
			const std::vector<uint8_t> original;
		};
		static const std::vector<tPatchBytes> _allPatches;

		static std::filesystem::path modulePath;
		static std::vector<std::filesystem::path> modsPaths;
		static bool logAccess;

		static VBFReader _vbf;
	};
}
