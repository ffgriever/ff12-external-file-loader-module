#include "pch.h"
#include "module.h"
#include "version.h"

const char* FF12HgetName()
{
	static const char moduleName[] = "FF12 External File Loader by ffgriever";
	return moduleName;
}

tVersion FF12HgetVer()
{
	return {VER_MAJOR, VER_MINOR, VER_STEP};
}
