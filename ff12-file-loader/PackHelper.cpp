#include "pch.h"
#include "PackHelper.h"

namespace ff12
{
	//All the files are loaded only once, so implementation doesn't matter much
	HANDLE PackHelper::makePackedFile(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		if (baseDirs.size() < 1)
			return INVALID_HANDLE_VALUE;
		std::wstring filename = baseDirs[0].filename();
		if (filename == L"battle_pack.bin.dir")
			return buildBattlePack(baseDirs, 71, baseFile, { {61, 15} });
		else if (filename == L"word.bin.dir")
			return buildBattlePack(baseDirs, 15, baseFile);
		else if (filename.size() > 8 && filename.substr(filename.size() - 8) == L".ebp.dir")
			return buildEbp(baseDirs, baseFile);
		else if (filename.size() > 8 && filename.substr(filename.size() - 8) == L".ard.dir")
			return buildArd(baseDirs, baseFile);
		else
			return buildPack(baseDirs, getExt(filename), baseFile);
	}
	size_t PackHelper::getPackedFileSize(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		if (baseDirs.size() < 1)
			return 0;
		std::wstring filename = baseDirs[0].filename();
		if (filename == L"battle_pack.bin.dir")
			return getSizeBattlePack(baseDirs, 71, baseFile, { {61, 15} });
		else if (filename == L"word.bin.dir")
			return getSizeBattlePack(baseDirs, 15, baseFile);
		else if (filename.size() > 8 && filename.substr(filename.size() - 8) == L".ebp.dir")
			return getSizeEbp(baseDirs, baseFile);
		else if (filename.size() > 8 && filename.substr(filename.size() - 8) == L".ard.dir")
			return getSizeArd(baseDirs, baseFile);
		else
			return getSizePack(baseDirs, getExt(filename), baseFile);
	}
	std::wstring PackHelper::getExt(const std::wstring& filename)
	{
		std::wstring ret = L"bin";
		if (
			filename == L"texpack_ys.bin.dir"
			|| filename == L"tex2pack_ys.bin.dir"
			|| filename == L"fontpack_fs.bin.dir"
			|| filename == L"fontpack_it.bin.dir"
			)
			ret = L"tm2";
		else if (filename == L"mrppack_ys.bin.dir")
			ret = L"mrp";
		else if (filename == L"clutpack_ys.bin.dir")
			ret = L"clt2";
		return ret;
	}
	uint32_t PackHelper::countFiles(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext)
	{
		uint32_t cnt = 0;
		bool found;
		do
		{
			found = false;
			for (const auto& dir : baseDirs)
			{
				if (std::filesystem::exists(dir / getSectionName(cnt, ext)))
				{
					cnt++;
					found = true;
					break;
				}
			}		
		} while (found);
		return cnt;
	}
	std::filesystem::path PackHelper::findFile(const std::vector<std::filesystem::path>& baseDirs, std::filesystem::path file)
	{
		std::filesystem::path ret;
		for (const auto& dir : baseDirs)
		{
			std::filesystem::path path = dir / file;
			if (std::filesystem::exists(path))
			{
				ret = path;
				break;
			}
		}
		return ret;
	}
	HANDLE PackHelper::buildPack(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext, const VBFReader::unpackedFile& baseFile)
	{
		HANDLE hFile = INVALID_HANDLE_VALUE;

		const uint32_t* basePointers = reinterpret_cast<const uint32_t*>(baseFile.data.get());
		uint32_t cnt = 0;
		if (baseFile.data)
		{
			if (basePointers[0] < baseFile.size)
			{
				const uint32_t maxCount = basePointers[0] / sizeof(basePointers[0]);
				for (cnt = 0; cnt < maxCount; cnt++)
					if (basePointers[cnt] == 0xffffffff) break;
			}
		}
		else
		{
			cnt = countFiles(baseDirs, ext);
		}
		if (!cnt) return hFile;

		uint32_t headerSize = ((cnt + 0xf) >> 4) << 4;

		std::unique_ptr<uint32_t[]> header(new uint32_t[headerSize]);
		memset(header.get(), 0xff, headerSize * 4);

		try
		{
			DWORD bytesCount = 0;
			wchar_t tmpName[L_tmpnam_s];
			_wtmpnam_s(tmpName);
			hFile = CreateFileW(tmpName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE, nullptr);
			if (hFile == INVALID_HANDLE_VALUE) return hFile;

			SetFilePointer(hFile, headerSize * 4, nullptr, FILE_BEGIN);

			uint32_t* pointer = header.get();
			for (size_t num = 0; num < cnt; num++)
			{
				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num, ext));

				if (unpackedFile.empty())
				{
					if (!baseFile.data)
						throw std::runtime_error("File not present in any of the unpacked directories and no base file specified.");

					uint32_t fsize = 0;

					if (num == cnt - 1 || basePointers[num + 1] == 0xffffffff)
						fsize = baseFile.size - basePointers[num];
					else
						fsize = basePointers[num+1] - basePointers[num];

					pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);

					WriteFile(hFile, &baseFile.data[basePointers[num]], fsize, &bytesCount, nullptr);
				}
				else
				{
					if (!std::filesystem::exists(unpackedFile) || std::filesystem::is_directory(unpackedFile) || std::filesystem::file_size(unpackedFile) == 0)
						break;

					auto fsize = std::filesystem::file_size(unpackedFile);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[fsize]);
					pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);

					HANDLE unpackedHandle = CreateFileW(unpackedFile.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (unpackedHandle == INVALID_HANDLE_VALUE)
						break;
					ReadFile(unpackedHandle, buffer.get(), fsize, &bytesCount, nullptr);
					CloseHandle(unpackedHandle);

					WriteFile(hFile, buffer.get(), fsize, &bytesCount, nullptr);
				}

				auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
				roundPos = ((roundPos + 0x3f) >> 6) << 6;
				SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
			}
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);

			WriteFile(hFile, header.get(), headerSize * 4, &bytesCount, nullptr);
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
		}
		catch (std::runtime_error&)
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);
				hFile = INVALID_HANDLE_VALUE;
			}
		}
		return hFile;
	}
	size_t PackHelper::getSizePack(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext, const VBFReader::unpackedFile& baseFile)
	{
		size_t ret = 0;

		const uint32_t* basePointers = reinterpret_cast<const uint32_t*>(baseFile.data.get());
		uint32_t cnt = 0;
		if (baseFile.data)
		{
			if (basePointers[0] < baseFile.size)
			{
				const uint32_t maxCount = basePointers[0] / sizeof(basePointers[0]);
				for (cnt = 0; cnt < maxCount; cnt++)
					if (basePointers[cnt] == 0xffffffff) break;
			}
		}
		else
		{
			cnt = countFiles(baseDirs, ext);
		}
		if (!cnt) return 0;

		uint32_t headerSize = ((cnt + 0xf) >> 4) << 4;

		try
		{
			ret += headerSize * 4;

			for (size_t num = 0; num < cnt; num++)
			{
				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num, ext));

				if (unpackedFile.empty())
				{
					if (!baseFile.data)
						throw std::runtime_error("File not present in any of the unpacked directories and no base file specified.");

					uint32_t fsize = 0;

					if (num == cnt - 1 || basePointers[num + 1] == 0xffffffff)
						fsize = baseFile.size - basePointers[num];
					else
						fsize = basePointers[num + 1] - basePointers[num];

					ret += fsize;
				}
				else
				{
					if (!std::filesystem::exists(unpackedFile) || std::filesystem::is_directory(unpackedFile) || std::filesystem::file_size(unpackedFile) == 0)
						break;

					auto fsize = std::filesystem::file_size(unpackedFile);

					ret += fsize;
				}

				ret = ((ret + 0x3f) >> 6) << 6;
			}
		}
		catch (std::runtime_error&)
		{
			ret = 0;
		}
		return ret;
	}
	HANDLE PackHelper::buildBattlePack(const std::vector<std::filesystem::path>& baseDirs, uint32_t maxSections, const VBFReader::unpackedFile& baseFile, const std::map<uint32_t, uint32_t>& packed)
	{
		HANDLE hFile = INVALID_HANDLE_VALUE;
		if (maxSections == 0)
			return hFile;

		uint32_t headerSize = maxSections + 2;

		std::unique_ptr<uint32_t[]> header(new uint32_t[headerSize]);
		memset(header.get(), 0, headerSize * 4);
		try
		{
			DWORD bytesCount = 0;
			wchar_t tmpName[L_tmpnam_s];
			_wtmpnam_s(tmpName);
			hFile = CreateFileW(tmpName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE, nullptr);
			if (hFile == INVALID_HANDLE_VALUE) return hFile;
			SetFilePointer(hFile, (((headerSize * 4) + 7) >> 3) << 3, nullptr, FILE_BEGIN);

			*header.get() = maxSections;
			uint32_t* pointer = header.get() + 1;
			for (uint32_t num = 0; num < maxSections; num++)
			{
				std::wstring sectionName = getSectionName(num);

				std::filesystem::path unpackedFile;
				for (const auto& path : baseDirs)
				{
					std::filesystem::path file = path / sectionName;
					std::filesystem::path dir = path / (sectionName + L".dir");
					if (std::filesystem::exists(dir) && std::filesystem::is_directory(dir))
					{
						unpackedFile = dir;
						break;
					}
					else if (std::filesystem::exists(file))
					{
						unpackedFile = file;
						break;
					}
				}

				if (unpackedFile.empty() || (!std::filesystem::is_directory(unpackedFile) && std::filesystem::file_size(unpackedFile) == 0))
				{
					if (baseFile.data)
					{
						uint32_t* basePointers = reinterpret_cast<uint32_t*>(baseFile.data.get() + 4);
						if (basePointers[num] == basePointers[num + 1])
						{
							pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
						}
						else
						{
							uint32_t fsize = basePointers[num + 1] - basePointers[num];
							pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);

							WriteFile(hFile, &baseFile.data[basePointers[num]], fsize, &bytesCount, nullptr);

							auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
							roundPos = ((roundPos + 3) >> 2) << 2;
							SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
						}
					}
					else
					{
						pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
					}
				}
				else if (std::filesystem::is_directory(unpackedFile))
				{
					pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);

					const auto entry = packed.find(num);
					if (entry == packed.end())
						throw std::runtime_error("Section " + std::to_string(num) + " should not be an archive!");

					std::vector<std::filesystem::path> baseSubdirs;
					for (const auto& dir : baseDirs)
					{
						auto subDir = dir / (sectionName + L".dir");
						if (std::filesystem::exists(subDir))
							baseSubdirs.push_back(subDir);
					}
					VBFReader::unpackedFile baseSubfile;
					if (baseFile.data)
					{
						uint32_t* basePointers = reinterpret_cast<uint32_t*>(baseFile.data.get() + 4);
						size_t size = basePointers[num + 1] - basePointers[num];
						baseSubfile.data.reset(new uint8_t[size]);
						baseSubfile.size = size;
						memcpy(baseSubfile.data.get(), &baseFile.data[basePointers[num]], size);
					}
					HANDLE archive = buildBattlePack(baseSubdirs, entry->second, baseSubfile);
					if (archive == INVALID_HANDLE_VALUE)
						throw std::runtime_error("Error opening section " + std::to_string(num) + " as an archive!");

					auto archiveSize = SetFilePointer(archive, 0, nullptr, FILE_END);
					SetFilePointer(archive, 0, nullptr, FILE_BEGIN);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[archiveSize]);

					ReadFile(archive, buffer.get(), archiveSize, &bytesCount, nullptr);
					CloseHandle(archive);

					WriteFile(hFile, buffer.get(), archiveSize, &bytesCount, nullptr);

					auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
					roundPos = ((roundPos + 3) >> 2) << 2;
					SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
				}
				else
				{
					auto fsize = std::filesystem::file_size(unpackedFile);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[fsize]);
					pointer[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);

					HANDLE unpackedHandle = CreateFileW(unpackedFile.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (unpackedHandle == INVALID_HANDLE_VALUE)
					{
						throw std::runtime_error("Error opening section " + std::to_string(num) + "!");
					}
					else
					{
						ReadFile(unpackedHandle, buffer.get(), fsize, &bytesCount, nullptr);
						CloseHandle(unpackedHandle);
						WriteFile(hFile, buffer.get(), fsize, &bytesCount, nullptr);

						auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
						roundPos = ((roundPos + 3) >> 2) << 2;
						SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
					}
				}
			}
			pointer[maxSections] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
			WriteFile(hFile, header.get(), headerSize * 4, &bytesCount, nullptr);
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
		}
		catch (std::runtime_error&)
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);
				hFile = INVALID_HANDLE_VALUE;
			}
		}
		return hFile;
	}
	size_t PackHelper::getSizeBattlePack(const std::vector<std::filesystem::path>& baseDirs, uint32_t maxSections, const VBFReader::unpackedFile& baseFile, const std::map<uint32_t, uint32_t>& packed)
	{
		size_t ret = 0;
		if (maxSections == 0)
			return ret;

		uint32_t headerSize = maxSections + 2;

		try
		{
			ret += headerSize * 4;
			ret = ((ret + 7) >> 3) << 3;

			for (uint32_t num = 0; num < maxSections; num++)
			{
				std::wstring sectionName = getSectionName(num);

				std::filesystem::path unpackedFile;
				for (const auto& path : baseDirs)
				{
					std::filesystem::path file = path / sectionName;
					std::filesystem::path dir = path / (sectionName + L".dir");
					if (std::filesystem::exists(dir) && std::filesystem::is_directory(dir))
					{
						unpackedFile = dir;
						break;
					}
					else if (std::filesystem::exists(file))
					{
						unpackedFile = file;
						break;
					}
				}

				if (unpackedFile.empty() || (!std::filesystem::is_directory(unpackedFile) && std::filesystem::file_size(unpackedFile) == 0))
				{
					if (baseFile.data)
					{
						uint32_t* basePointers = reinterpret_cast<uint32_t*>(baseFile.data.get() + 4);
						if (basePointers[num] != basePointers[num + 1])
						{
							uint32_t fsize = basePointers[num + 1] - basePointers[num];
							ret += fsize;
							ret = ((ret + 3) >> 2) << 2;
						}
					}
				}
				else if (std::filesystem::is_directory(unpackedFile))
				{
					const auto entry = packed.find(num);
					if (entry == packed.end())
						throw std::runtime_error("Section " + std::to_string(num) + " should not be an archive!");

					std::vector<std::filesystem::path> baseSubdirs;
					for (const auto& dir : baseDirs)
					{
						auto subDir = dir / (sectionName + L".dir");
						if (std::filesystem::exists(subDir))
							baseSubdirs.push_back(subDir);
					}
					VBFReader::unpackedFile baseSubfile;
					if (baseFile.data)
					{
						uint32_t* basePointers = reinterpret_cast<uint32_t*>(baseFile.data.get() + 4);
						size_t size = basePointers[num + 1] - basePointers[num];
						baseSubfile.data.reset(new uint8_t[size]);
						baseSubfile.size = size;
						memcpy(baseSubfile.data.get(), &baseFile.data[basePointers[num]], size);
					}
					size_t archiveSize = getSizeBattlePack(baseSubdirs, entry->second, baseSubfile);
					if (archiveSize == 0)
						throw std::runtime_error("Error opening section " + std::to_string(num) + " as an archive!");

					ret += archiveSize;
					ret = ((ret + 3) >> 2) << 2;
				}
				else
				{
					auto fsize = std::filesystem::file_size(unpackedFile);
					ret += fsize;
					ret = ((ret + 3) >> 2) << 2;
				}
			}
		}
		catch (std::runtime_error&)
		{
			ret = 0;
		}
		return ret;
	}

	HANDLE PackHelper::buildEbp(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		HANDLE hFile = INVALID_HANDLE_VALUE;

		const EBPHeader* baseHeader = reinterpret_cast<const EBPHeader*>(baseFile.data.get());
		uint32_t cnt = 20;
	
		EBPHeader header;

		try
		{
			DWORD bytesCount = 0;
			wchar_t tmpName[L_tmpnam_s];
			_wtmpnam_s(tmpName);
			hFile = CreateFileW(tmpName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE, nullptr);
			if (hFile == INVALID_HANDLE_VALUE) return hFile;

			SetFilePointer(hFile, sizeof(header) + 32, nullptr, FILE_BEGIN);

			for (size_t num = 0; num < cnt; num++)
			{
				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num));

				if (unpackedFile.empty())
				{
					if (!baseFile.data)
					{
						header.pointers[num] = 0;
						continue;
					}

					uint32_t fsize = 0;

					if (baseHeader->pointers[num] == 0)
					{
						header.pointers[num] = 0;
						continue;
					}

					for (size_t next = num + 1; next < cnt; next++)
					{
						if (baseHeader->pointers[next] != 0)
						{
							fsize = baseHeader->pointers[next] - baseHeader->pointers[num];
							break;
						}
					}
					if (fsize == 0)
						fsize = baseFile.size - baseHeader->pointers[num];

					header.pointers[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;
					WriteFile(hFile, &baseFile.data[baseHeader->pointers[num]], fsize, &bytesCount, nullptr);
				}
				else
				{
					if (std::filesystem::is_directory(unpackedFile) || std::filesystem::file_size(unpackedFile) == 0)
						break;

					auto fsize = std::filesystem::file_size(unpackedFile);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[fsize]);

					header.pointers[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;

					HANDLE unpackedHandle = CreateFileW(unpackedFile.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (unpackedHandle == INVALID_HANDLE_VALUE)
						break;
					ReadFile(unpackedHandle, buffer.get(), fsize, &bytesCount, nullptr);
					CloseHandle(unpackedHandle);
					WriteFile(hFile, buffer.get(), fsize, &bytesCount, nullptr);
				}

				auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
				roundPos = ((roundPos + 0x3f) >> 6) << 6;
				SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
			}
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
			WriteFile(hFile, &header, sizeof(header), &bytesCount, nullptr);
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
		}
		catch (std::runtime_error&)
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);
				hFile = INVALID_HANDLE_VALUE;
			}
		}
		return hFile;
	}
	size_t PackHelper::getSizeEbp(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		size_t ret = 0;

		const EBPHeader* baseHeader = reinterpret_cast<const EBPHeader*>(baseFile.data.get());
		uint32_t cnt = 20;

		try
		{
			ret += sizeof(EBPHeader) + 32;

			for (size_t num = 0; num < cnt; num++)
			{
				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num));

				if (unpackedFile.empty())
				{
					if (!baseFile.data)
						continue;

					uint32_t fsize = 0;

					if (baseHeader->pointers[num] == 0)
						continue;

					for (size_t next = num + 1; next < cnt; next++)
					{
						if (baseHeader->pointers[next] != 0)
						{
							fsize = baseHeader->pointers[next] - baseHeader->pointers[num];
							break;
						}
					}
					if (fsize == 0)
						fsize = baseFile.size - baseHeader->pointers[num];

					ret += fsize;
				}
				else
				{
					if (std::filesystem::is_directory(unpackedFile) || std::filesystem::file_size(unpackedFile) == 0)
						break;

					ret += std::filesystem::file_size(unpackedFile);
				}

				ret = ((ret + 0x3f) >> 6) << 6;
			}
		}
		catch (std::runtime_error&)
		{
			ret = 0;
		}
		return ret;
	}
	HANDLE PackHelper::buildArd(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		HANDLE hFile = INVALID_HANDLE_VALUE;

		const ARDHeader* baseHeader = reinterpret_cast<const ARDHeader*>(baseFile.data.get());
		constexpr uint32_t cnt = 10;

		ARDHeader header{};

		try
		{
			DWORD bytesCount = 0;
			wchar_t tmpName[L_tmpnam_s];
			_wtmpnam_s(tmpName);
			hFile = CreateFileW(tmpName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE, nullptr);
			if (hFile == INVALID_HANDLE_VALUE)
			{
				LOG(WARN) << "Error creating temporary file for ARD";
				return hFile;
			}

			SetFilePointer(hFile, sizeof(header), nullptr, FILE_BEGIN);

			for (size_t num = 2; num < cnt; num++)
			{
				if (num == 5 || num == 6)
				{
					header.pointers[num] = 0;
					continue;
				}
				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num));

				if (unpackedFile.empty())
				{
					if (!baseFile.data)
					{
						header.pointers[num] = 0;
						continue;
					}

					uint32_t fsize = 0;

					if (baseHeader->pointers[num] == 0)
					{
						header.pointers[num] = 0;
						continue;
					}

					for (size_t next = num + 1; next < cnt; next++)
					{
						if (baseHeader->pointers[next] != 0)
						{
							fsize = baseHeader->pointers[next] - baseHeader->pointers[num];
							break;
						}
					}
					if (fsize == 0)
					{
						if (baseHeader->pointers[1] != 0 && baseHeader->pointers[1] > baseHeader->pointers[num])
						{
							fsize = baseHeader->pointers[1] - baseHeader->pointers[num]; //section one should always be after section nine
						}
						else
						{
							fsize = baseFile.size - baseHeader->pointers[num]; //it is the last section present
						}
					}

					header.pointers[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;
					bool writeResult = WriteFile(hFile, &baseFile.data[baseHeader->pointers[num]], fsize, &bytesCount, nullptr);
					if (!writeResult || bytesCount != fsize)
					{
						DWORD lastErr = GetLastError();
						LOG(WARN) << "Error writing packed section " << num << " to ARD. Result: " << writeResult << ":" << lastErr << " count: " << bytesCount << "/" << fsize;
						LOG(WARN) << "baseFile.size: " << baseFile.size << " baseHeader->pointers[1]: " << baseHeader->pointers[1];
						CloseHandle(hFile);
						hFile = INVALID_HANDLE_VALUE;
						return hFile;
					}
				}
				else
				{
					if (std::filesystem::is_directory(unpackedFile) || std::filesystem::file_size(unpackedFile) == 0)
					{
						LOG(INFO) << "Section " << num << " is empty or a directory";
						break;
					}

					auto fsize = std::filesystem::file_size(unpackedFile);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[fsize]);

					header.pointers[num] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;

					HANDLE unpackedHandle = CreateFileW(unpackedFile.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (unpackedHandle == INVALID_HANDLE_VALUE)
					{
						LOG(WARN) << "Error opening section " << num << " file";
						break;
					}
					bool readResult = ReadFile(unpackedHandle, buffer.get(), fsize, &bytesCount, nullptr);
					if (!readResult || bytesCount != fsize)
					{
						LOG(WARN) << "Error reading unpacked section " << num << " file. Result: " << readResult << "count: " << bytesCount << "/" << fsize;
					}
					CloseHandle(unpackedHandle);
					bool writeResult = WriteFile(hFile, buffer.get(), fsize, &bytesCount, nullptr);
					if (!writeResult || bytesCount != fsize)
					{
						DWORD lastErr = GetLastError();
						LOG(WARN) << "Error writing unpacked section " << num << " to ARD. Result: " << writeResult << ":" << lastErr << " count: " << bytesCount << "/" << fsize;
						CloseHandle(hFile);
						hFile = INVALID_HANDLE_VALUE;
						return hFile;
					}
				}

				auto roundPos = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);
				roundPos = ((roundPos + 0xf) >> 4) << 4;
				SetFilePointer(hFile, roundPos, nullptr, FILE_BEGIN);
			}

			//handle section one as in original
			std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(1));
			header.pointers[1] = 0;

			if (unpackedFile.empty())
			{
				if (baseFile.data && baseHeader->pointers[1] != 0)
				{
					uint32_t fsize = 0;

					for (size_t num = 2; num < cnt; num++)
					{
						if (baseHeader->pointers[num] != 0 && baseHeader->pointers[num] > baseHeader->pointers[1])
						{
							fsize = baseHeader->pointers[num] - baseHeader->pointers[1];
							break;
						}
					}
					if (fsize == 0)
					{
						fsize = baseFile.size - baseHeader->pointers[1];
					}

					header.pointers[1] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;
					bool writeResult = WriteFile(hFile, &baseFile.data[baseHeader->pointers[1]], fsize, &bytesCount, nullptr);
					if (!writeResult || bytesCount != fsize)
					{
						DWORD lastErr = GetLastError();
						LOG(WARN) << "Error writing packed section one to ARD. Result: " << writeResult << ":" << lastErr << " count: " << bytesCount << "/" << fsize;
						LOG(WARN) << "baseFile.size: " << baseFile.size << " baseHeader->pointers[1]: " << baseHeader->pointers[1];
						CloseHandle(hFile);
						hFile = INVALID_HANDLE_VALUE;
						return hFile;
					}
				}
			}
			else
			{
				if (!std::filesystem::is_directory(unpackedFile) && std::filesystem::file_size(unpackedFile) != 0)
				{
					auto fsize = std::filesystem::file_size(unpackedFile);
					std::unique_ptr<uint8_t[]> buffer(new uint8_t[fsize]);

					header.pointers[1] = SetFilePointer(hFile, 0, nullptr, FILE_CURRENT);;

					HANDLE unpackedHandle = CreateFileW(unpackedFile.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (unpackedHandle != INVALID_HANDLE_VALUE)
					{
						bool readResult = ReadFile(unpackedHandle, buffer.get(), fsize, &bytesCount, nullptr);
						if (!readResult || bytesCount != fsize)
						{
							LOG(WARN) << "Error reading unpacked section one file. Result: " << readResult << " count: " << bytesCount << "/" << fsize;
						}
						CloseHandle(unpackedHandle);
						bool writeResult = WriteFile(hFile, buffer.get(), fsize, &bytesCount, nullptr);
						if (!writeResult || bytesCount != fsize)
						{
							DWORD lastErr = GetLastError();
							LOG(WARN) << "Error writing unpacked section one to ARD. Result: " << writeResult << ":" << lastErr << " count: " << bytesCount << "/" << fsize;
							CloseHandle(hFile);
							hFile = INVALID_HANDLE_VALUE;
							return hFile;
						}
					}
				}
			}
			//end section one

			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
			bool headerResult = WriteFile(hFile, &header, sizeof(header), &bytesCount, nullptr);
			if (!headerResult || bytesCount != sizeof(header))
			{
				DWORD lastErr = GetLastError();
				LOG(WARN) << "Error writing header to ARD. Result: " << headerResult << ":" << lastErr << " count: " << bytesCount << "/" << sizeof(header);
				CloseHandle(hFile);
				hFile = INVALID_HANDLE_VALUE;
				return hFile;
			}
			SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
		}
		catch (std::runtime_error&)
		{
			LOG(WARN) << "Error building ARD: " << hFile;
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);
				hFile = INVALID_HANDLE_VALUE;
			}
		}
		return hFile;
	}
	size_t PackHelper::getSizeArd(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile)
	{
		size_t ret = 0;

		const ARDHeader* baseHeader = reinterpret_cast<const ARDHeader*>(baseFile.data.get());
		constexpr uint32_t cnt = 10;

		try
		{
			ret += sizeof(ARDHeader);
			size_t sectionOne = 0;

			for (size_t num = 1; num < cnt; num++)
			{
				if (num == 5 || num == 6)
					continue;

				std::filesystem::path unpackedFile = findFile(baseDirs, getSectionName(num));

				if (unpackedFile.empty())
				{
					if (baseFile.data)
					{

						uint32_t fsize = 0;

						if (baseHeader->pointers[num] == 0)
						{
							continue;
						}

						if (num == 1 && baseHeader->pointers[1] != 0)
						{
							fsize = 0;
							for (size_t next = 2; next < cnt; next++)
							{
								if (baseHeader->pointers[next] != 0 && baseHeader->pointers[next] > baseHeader->pointers[1])
								{
									fsize = baseHeader->pointers[next] - baseHeader->pointers[1];
									break;
								}
							}
							if (fsize == 0)
							{
								fsize = baseFile.size - baseHeader->pointers[1];
							}
						}
						else
						{
							for (size_t next = num + 1; next < cnt; next++)
							{
								if (baseHeader->pointers[next] != 0)
								{
									fsize = baseHeader->pointers[next] - baseHeader->pointers[num];
									break;
								}
							}
							if (fsize == 0)
							{
								if (baseHeader->pointers[1] != 0 && baseHeader->pointers[1] > baseHeader->pointers[num])
									fsize = baseHeader->pointers[1] - baseHeader->pointers[num]; //section one is always after section nine
								else
									fsize = baseFile.size - baseHeader->pointers[num];
							}
						}

						num == 1 ? sectionOne = fsize : ret += fsize;
					}
				}
				else
				{
					if (std::filesystem::is_directory(unpackedFile) || (std::filesystem::file_size(unpackedFile) == 0 && num != 1))
						break;

					size_t fsize = std::filesystem::file_size(unpackedFile);
					num == 1 ? sectionOne = fsize : ret += fsize;
				}
				ret = ((ret + 0xf) >> 4) << 4;
			}
			ret += sectionOne; //special treatment
		}
		catch (std::runtime_error&)
		{
			ret = 0;
		}
		return ret;
	}
	std::wstring PackHelper::getSectionName(size_t num, const std::wstring& ext)
	{
		std::wstringstream ret;
		ret << L"section_" << std::setfill(L'0') << std::setw(3) << num << L"." << (ext.size() > 0 ? ext : L"bin");
		return ret.str();
	}
}
