#include "pch.h"
#include "VBFReader.h"
#include <zlib.h>

namespace ff12
{

	VBFReader::VBFReader(const std::wstring& path)
		: _filename(path)
	{
		open(path);
	}

	VBFReader::unpackedFile VBFReader::unpack(const std::string& inFile, uint32_t blocks)
	{
		unpackedFile ret;
		if (!_file)
			return ret;

		std::string file = _getNormalizedPath(inFile);

		const auto entry = _vbfEntries.find(file);
		if (entry != _vbfEntries.cend())
		{
			ret.data.reset(new uint8_t[blocks > 0 ? _max_block_size * blocks : entry->second.originalSize]);
			ret.size = entry->second.originalSize;
			_file.seekg(entry->second.startOffset, std::ios::beg);

			std::unique_ptr<uint8_t[]> ioBuffer(new uint8_t[_max_block_size]);

			uint32_t blockCount = 0;
			uint32_t bufferPos = 0;
			for (uint32_t blockSize : entry->second.blocklist)
			{
				if (blocks > 0 && blockCount >= blocks) break;
				if (blockSize == 0) blockSize = _max_block_size;

				_file.read(reinterpret_cast<char*>(ioBuffer.get()), blockSize);

				if (blockSize == _max_block_size || (blockCount == entry->second.blocklist.size() - 1 && blockSize == entry->second.originalSize % _max_block_size))
				{
					memcpy(&ret.data[bufferPos], ioBuffer.get(), blockSize);
					bufferPos += blockSize;
				}
				else
				{
					unsigned long outputSize = static_cast<unsigned long>(entry->second.originalSize - bufferPos);
					int result = uncompress(&ret.data[bufferPos], &outputSize, &ioBuffer[0], blockSize);
					bufferPos += outputSize;
					if (result != Z_OK)
						throw VBFReaderException("Problem decompressing file " + file);
				}
				blockCount++;
			}
		}
		return ret;
	}

	bool VBFReader::fileExists(const std::string& inFile)
	{
		if (!_file)
			return false;

		std::string file = _getNormalizedPath(inFile);

		const auto entry = _vbfEntries.find(file);
		if (entry != _vbfEntries.cend())
			return true;
		else
			return false;
	}

	VBFReader::~VBFReader()
	{
		if (_file)
			_file.close();
	}

	VBFReader::operator bool() const
	{
		return static_cast<bool>(_file);
	}

	bool VBFReader::open(const std::wstring& path)
	{
		try
		{
			if (_file)
				_reset();

			_filename = path;
			_file.open(path, std::ios::binary);

			if (!_file)
				return false;

			VBF_HEADER vbfHead;
			_file.read(reinterpret_cast<char*>(&vbfHead), sizeof(vbfHead));
			if (vbfHead.magic != 0x4b595253) throw VBFReaderException("Invalid VBF file format");

			//skip md5 checksums
			_file.seekg(16 * vbfHead.numOfFiles, std::ios::cur);

			//file index
			std::vector<VBF_FILE> vbfIndex(vbfHead.numOfFiles);
			_file.read(reinterpret_cast<char*>(vbfIndex.data()), sizeof(vbfIndex[0]) * vbfIndex.size());

			//name table
			uint32_t vbfStringTableSize;
			_file.read(reinterpret_cast<char*>(&vbfStringTableSize), sizeof(vbfStringTableSize));
			std::vector<char> vbfStringTable(vbfStringTableSize - 4);
			_file.read(vbfStringTable.data(), vbfStringTableSize - 4);

			//block index
			uint32_t vbfBlockCount = _getBlockCount(vbfIndex);
			std::vector<uint16_t> vbfBlockIndex(vbfBlockCount);
			_file.read(reinterpret_cast<char*>(vbfBlockIndex.data()), sizeof(vbfBlockIndex[0]) * vbfBlockIndex.size());

			//Keeping that in memory will take around 20MB, but will make it much faster to find files later.
			_populateEntries(vbfIndex, vbfStringTable.data(), vbfBlockIndex);

			return true;
		}
		catch (VBFReaderException&)
		{
			_reset();
			return false;
		}
	}

	void VBFReader::close()
	{
		_reset();
	}

	uint32_t VBFReader::_getBlockCount(const std::vector<VBF_FILE>& vbfIndex)
	{
		uint32_t blockCount = 0;
		for (const auto& index : vbfIndex)
		{
			blockCount += index.originalSize / _max_block_size;
			if ((index.originalSize % _max_block_size) != 0) blockCount++;
		}
		return blockCount;
	}

	void VBFReader::_populateEntries(const std::vector<VBF_FILE>& vbfIndex, const char* stringTable, const std::vector<uint16_t>& vbfBlockIndex)
	{
		for (const auto& index : vbfIndex)
		{
			auto element = _vbfEntries.emplace(&stringTable[index.filenameOffset], VBF_ENTRY{ index.originalSize, index.startOffset });

			uint32_t blockCount = index.originalSize / _max_block_size;
			if ((index.originalSize % _max_block_size) != 0) blockCount++;

			for (uint32_t i = index.blocklistStart; i < index.blocklistStart + blockCount; i++)
				element.first->second.blocklist.push_back(vbfBlockIndex[i]);
		}
	}

	void VBFReader::_reset()
	{
		if (_file)
			_file.close();
		_filename.clear();
		_vbfEntries.clear();
	}

	std::string VBFReader::_getNormalizedPath(const std::string& inFile)
	{
		std::string file = inFile;

		std::transform(file.begin(), file.end(), file.begin(),
			[](unsigned char c) -> unsigned char {
				if (c == '\\')
					return '/';
				else
					return std::tolower(c);
			});
		return file;
	}

}
