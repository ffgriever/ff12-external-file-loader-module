#pragma once
#include "VBFReader.h"

namespace ff12
{

	class PackHelper
	{
	public:
		static HANDLE makePackedFile(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
		static size_t getPackedFileSize(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
	private:
		struct EBPHeader
		{
			uint32_t magic = 0x32504245;
			uint32_t num_of_sections = 20;
			uint32_t padding[2] = {0, 0};
			uint32_t pointers[20];
		};
		struct ARDHeader
		{
			uint64_t magic = ARD_MAGIC;
			uint32_t pointers[10];
		};
		static constexpr uint64_t ARD_MAGIC = 0x3330524132314646ULL;

		static std::wstring getExt(const std::wstring& filename);
		static uint32_t countFiles(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext);
		static std::filesystem::path findFile(const std::vector<std::filesystem::path>& baseDirs, std::filesystem::path file);
		static HANDLE buildPack(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext, const VBFReader::unpackedFile& baseFile);
		static size_t getSizePack(const std::vector<std::filesystem::path>& baseDirs, const std::wstring& ext, const VBFReader::unpackedFile& baseFile);
		static HANDLE buildBattlePack(const std::vector<std::filesystem::path>& baseDirs, uint32_t maxSections, const VBFReader::unpackedFile& baseFile, const std::map<uint32_t, uint32_t>& packed = {});
		static size_t getSizeBattlePack(const std::vector<std::filesystem::path>& baseDirs, uint32_t maxSections, const VBFReader::unpackedFile& baseFile, const std::map<uint32_t, uint32_t>& packed = {});
		static HANDLE buildEbp(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
		static size_t getSizeEbp(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
		static HANDLE buildArd(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
		static size_t getSizeArd(const std::vector<std::filesystem::path>& baseDirs, const VBFReader::unpackedFile& baseFile);
		static std::wstring getSectionName(size_t num, const std::wstring& ext = L"");
	};

}
