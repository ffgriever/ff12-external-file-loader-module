?openPhyreHook@FileLoader@ff12@@CAPEAUstreamStructMain@2@PEAXPEADPEAUreadInfoStruct@2@@Z PROTO C

.code
; I needed to add a third parameter to that call, unfortunately x64 VSC++ doesn't support inline ASM nor naked functions
; The function is used only in one place, with desired value always in r15, so this workaround will work, even though it's not pretty
jumpToOpen proc
    mov r8, r15
    jmp ?openPhyreHook@FileLoader@ff12@@CAPEAUstreamStructMain@2@PEAXPEADPEAUreadInfoStruct@2@@Z
jumpToOpen endp
end
