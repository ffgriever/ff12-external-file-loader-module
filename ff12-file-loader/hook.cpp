#include "pch.h"
#include "hook.h"
#include "PackHelper.h"

namespace ff12
{
	VBFReader FileLoader::_vbf;
	std::filesystem::path FileLoader::modulePath;
	std::vector<std::filesystem::path> FileLoader::modsPaths = { "..\\mods\\unpacked" };
	bool FileLoader::logAccess = false;
	const std::vector<FileLoader::tHookInit> FileLoader::_allHooks = {
		{"hookOpenPhyre", FileLoader::OPENPHYRE, (intptr_t)&jumpToOpen, {0xE8, 0x49, 0xBD, 0xBF, 0xFF}},
		{"hookDestroyFreeMovie1", FileLoader::DESTROYFREEMOVIE1, (intptr_t)&FileLoader::destroyReaderMovieHook, {0xE8, 0xFF, 0x0B, 0xAA, 0xFF}},
		{"hookDestroyFreeMovie2", FileLoader::DESTROYFREEMOVIE2, (intptr_t)&FileLoader::destroyReaderMovieHook, {0xE8, 0x05, 0x68, 0xF9, 0xFF}},
		{"hookDestroyFreeMusic", FileLoader::DESTROYFREEMUSIC, (intptr_t)&FileLoader::destroyReaderMusicHook, {0xE8, 0x62, 0xC4, 0xFE, 0xFF}},
		{"hookDestroyReplace", FileLoader::DESTROYREPLACE, (intptr_t)&FileLoader::destroyReader, {0x48, 0x83, 0xEC, 0x38, 0x48}},
		{"hookGetSize1", FileLoader::GETSIZE1, (intptr_t)&FileLoader::getSizeHook, {0xE9, 0xEB, 0x1A, 0xE7, 0xFF}},
		{"hookGetSize2", FileLoader::GETSIZE2, (intptr_t)&FileLoader::getSizeHook, {0xE8, 0x01, 0x58, 0xED, 0xFF}},
		{"hookReadVBFById", FileLoader::READVBFBYID, (intptr_t)&FileLoader::readVBFByIdHook, {0xE8, 0x3D, 0x17, 0xE7, 0xFF}},
	};
	const std::vector<FileLoader::tPatchBytes> FileLoader::_allPatches = {
		{"emptyStreamReadPatch", FileLoader::EMPTYSTREAM, {0x74, 0x0b}, {0x74, 0x20}},
		{"destroyReplacePatch", FileLoader::DESTROYREPLACE, {0xE9, 0x00, 0x00, 0x00, 0x00, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90}, {0x48, 0x83, 0xEC, 0x38, 0x48, 0xC7, 0x44, 0x24, 0x20, 0xFE, 0xFF, 0xFF, 0xFF}},
		{"checkExistsPatch", FileLoader::CHECKEXISTSPATCH, {0x90, 0x90, 0x90, 0x90, 0x90, 0x90} , {0x0F, 0x84, 0x9E, 0x00, 0x00, 0x00}},
	};
	uintptr_t FileLoader::_moduleBase = 0x120000;

	bool FileLoader::testPatchesHooks()
	{
		for (const auto& patch : _allPatches)
		{
			if (patch.original.size() && memcmp((void*)getAddressASLR(patch.addr), patch.original.data(), patch.original.size()) != 0)
			{
				LOG(ERR) << "Invalid original bytes for " << patch.name;
				return false;
			}
		}
		for (const auto& hook : _allHooks)
		{
			if (memcmp((void*)getAddressASLR(hook.callPtr), hook.original, 5) != 0)
			{
				LOG(ERR) << "Invalid original bytes for " << hook.name;
				return false;
			}
		}
		return true;
	}

	bool FileLoader::initPatchesHooks()
	{
		if (!testPatchesHooks())
		{
			LOG(ERR) << "Invalid game version. Unable to find proper hook values.";
			return false;
		}

		HANDLE hProcess = GetCurrentProcess();

		for (const auto& patch : _allPatches)
		{
			if (!patch.patch.size()) continue;
			if (!WriteProcessMemory(hProcess, (LPVOID)getAddressASLR(patch.addr), patch.patch.data(), patch.patch.size(), NULL))
			{
				LOG(ERR) << "Unable to write " << patch.name;
				return false;
			}
		}
		for (const auto& hook : _allHooks)
		{
			const intptr_t hookAddr = intptr_t(hook.hookPtr);
			const intptr_t callPtr = getAddressASLR(hook.callPtr);
			int32_t callRelativeAddr = int32_t(hookAddr - (callPtr + 5));
			if (!WriteProcessMemory(hProcess, (LPVOID)(callPtr + 1), &callRelativeAddr, 4, NULL))
			{
				LOG(ERR) << "Unable to write " << hook.name;
				return false;
			}
		}
		
		//It is perfectly fine for it to be not present, etc.
		_vbf.open(modulePath / ".." / ".." / "FFXII_TZA.vbf");
		return true;
	}

	void FileLoader::setModulePath(const std::filesystem::path& path)
	{
		modulePath = path;
	}

	void FileLoader::readConfig()
	{
		std::wstring configPath = modulePath / "config" / "ff12-file-loader.ini";
		wchar_t buffer[512];
		if (GetPrivateProfileStringW(L"Logging", L"logAccess", L"false", buffer, _countof(buffer), configPath.c_str()))
		{
			if (std::wstring(buffer) == L"true")
				logAccess = true;
		}
		if (GetPrivateProfileStringW(L"Paths", nullptr, nullptr, buffer, _countof(buffer), configPath.c_str()))
		{
			modsPaths.clear();
			wchar_t* key = buffer;
			while (true)
			{
				wchar_t pathBuffer[1024];
				if (GetPrivateProfileStringW(L"Paths", key, nullptr, pathBuffer, _countof(pathBuffer), configPath.c_str()))
				{
					std::filesystem::path path(pathBuffer);
					if (path.is_relative())
						modsPaths.push_back(L".." / path);
					else
						modsPaths.push_back(path);
				}
				key += wcslen(key) + 1;
				if (*key == 0)
					break;
			}
		}
		if (!modsPaths.size())
			modsPaths.push_back(L"..\\mods\\unpacked");
	}

	void FileLoader::setModuleBase(uintptr_t address)
	{
		_moduleBase = address;
	}

	//do not propagate exceptions to a code compiled with a different compiler
	streamStructMain* FileLoader::openPhyreHook(void* hProv, char* filename, readInfoStruct* readInfo)
	{
		static const openPhyreType openPhyre = (openPhyreType)getAddressASLR(0x19AEE0);
		HANDLE hFile = INVALID_HANDLE_VALUE;
		try
		{
			for (auto pathIt = modsPaths.cbegin(); pathIt != modsPaths.cend(); ++pathIt)
			{
				const auto filePath = *pathIt / filename;
				const auto dirPath = *pathIt / (std::string(filename) + ".dir");
				if (std::filesystem::exists(dirPath))
				{
					//unpacked files
					if (logAccess) LOG(INFO) << "Reading DIRU: " << std::string(filename);

					auto baseFile = getBaseFile(filename, pathIt, modsPaths.cend());

					//list of lower priority locations containing unpacked directory
					std::vector<std::filesystem::path> baseDirs;
					baseDirs.push_back(dirPath);
					for (auto dirIt = pathIt + 1; dirIt != modsPaths.cend(); ++dirIt)
					{
						const auto path = *dirIt / (std::string(filename) + ".dir");
						if (std::filesystem::exists(path))
							baseDirs.push_back(path);
					}

					//it is fine if the vbf doesn't exist
					if ((!baseFile.data || baseFile.size == 0) && _vbf)
						baseFile = _vbf.unpack(filename);

					hFile = PackHelper::makePackedFile(baseDirs, baseFile);
					if (hFile != INVALID_HANDLE_VALUE)
					{
						readInfo->hFile = hFile;
						readInfo->bytesToRead = (uint32_t)SetFilePointer(hFile, 0, nullptr, FILE_END);
						SetFilePointer(hFile, 0, nullptr, FILE_BEGIN);
						return nullptr;
					}
				}
				else if (std::filesystem::exists(filePath))
				{
					if (logAccess) LOG(INFO) << "Reading DIR0: " << std::string(filename);
					hFile = CreateFileA(filePath.string().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
					if (hFile != INVALID_HANDLE_VALUE)
					{
						readInfo->hFile = hFile;
						readInfo->bytesToRead = (uint32_t)std::filesystem::file_size(filePath);
						return nullptr;
					}
				}
			}
			if (logAccess)
			{
				if (!_vbf.fileExists(filename))
					LOG(ERR) << "File does not exist (read): " << std::string(filename);
				else
					LOG(INFO) << "Reading VBF0: " << std::string(filename);
			}
			
		}
		catch (...)
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);
				readInfo->hFile = INVALID_HANDLE_VALUE;
			}
		}
		return openPhyre(hProv, filename);
	}

	void FileLoader::destroyReader(uint64_t* object)
	{
		object[0] = 0xD26C60;
		readInfoStruct* readInfo = (readInfoStruct*)&object[4];
		if (readInfo && readInfo->hFile != INVALID_HANDLE_VALUE)
		{
			CloseHandle(readInfo->hFile);
			readInfo->hFile = INVALID_HANDLE_VALUE;
		}
		if (object[2])
		{
			if (~(uint8_t)object[2] & 1)
				((void (*)(int64_t))0x584730)(object[2]);
		}
	}

	void FileLoader::destroyReaderMovieHook(readInfoStruct* readInfo)
	{
		if (readInfo && readInfo->hFile != INVALID_HANDLE_VALUE)
		{
			CloseHandle(readInfo->hFile);
			readInfo->hFile = INVALID_HANDLE_VALUE;
		}
	}

	void FileLoader::destroyReaderMusicHook(uintptr_t* object)
	{
		static const destroyMusicType destroyReaderMusic = (destroyMusicType)getAddressASLR(0x584730);
		if (object)
		{
			readInfoStruct* readInfo = (readInfoStruct*)&object[4];
			if (readInfo && readInfo->hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(readInfo->hFile);
				readInfo->hFile = INVALID_HANDLE_VALUE;
			}
		}
		destroyReaderMusic(object);
	}

	/*
	* This function is used only for malloc (PS2 files). Technically there
	* are other places where the size is read, but it's used to pass file
	* size to the open function, which I'm ignoring anyway for external files.
	*/
	int FileLoader::getSizeHook(int section, int index)
	{
		static const getSizeType getSize = (getSizeType)getAddressASLR(0x2DF960);
		static const getFileNameType getFileName = (getFileNameType)getAddressASLR(0x2DF980);
		const char* fileName = getFileName(section, index);

		if (fileName)
		{
			try
			{
				for (auto pathIt = modsPaths.cbegin(); pathIt != modsPaths.cend(); ++pathIt)
				{
					const std::wstring filePath = *pathIt / fileName;
					const std::wstring dirPath = filePath + L".dir";
					if (std::filesystem::exists(dirPath))
					{
						//we need the whole file only for battle pack, for all others we need only header, so first block will always be enough
						uint32_t blocks = 1;
						if (std::filesystem::path(filePath).filename() == L"battle_pack.bin")
							blocks = 0;

						//temporarily - these are loaded only once, so it won't be an issue, but a proper calculating won't be possible without
						//unpacking, so I guess a file will have to be repacked at this point and the result stored, so that real repack won't have
						//to repeat it.
						auto baseFile = getBaseFile(fileName, pathIt, modsPaths.cend(), blocks);

						//list of lower priority locations containing unpacked directory
						std::vector<std::filesystem::path> baseDirs;
						baseDirs.push_back(dirPath);
						for (auto dirIt = pathIt + 1; dirIt != modsPaths.cend(); ++dirIt)
						{
							const auto path = *dirIt / (std::string(fileName) + ".dir");
							if (std::filesystem::exists(path))
								baseDirs.push_back(path);
						}

						//it is fine if the vbf doesn't exist
						if ((!baseFile.data || baseFile.size == 0) && _vbf)
							baseFile = _vbf.unpack(fileName, blocks);

						int retval = (int)PackHelper::getPackedFileSize(baseDirs, baseFile);
						if (retval > 0)
							return retval;
					}
					else if (std::filesystem::exists(filePath))
					{
						int retval = (int)std::filesystem::file_size(filePath);
						//The sizes in FST are bigger as well, because game adds its own
						//data after some of them.
						if (filePath.substr(filePath.size() - 3) == L".cm")
							retval *= 2;
						return retval;
					}
				}
				if (logAccess)
				{
					if (!_vbf.fileExists(fileName)) LOG(ERR) << "File does not exist (size): " << std::string(fileName);
				}
			}
			catch (...) {}
		}
		return getSize(section, index);
	}

	int FileLoader::readVBFByIdHook(int section, int index, void* dest, unsigned int size, int startOffset)
	{
		static const readVBFByIdType readVBFById = (readVBFByIdType)getAddressASLR(0x2DF780);
		static const getSizeType getSize = (getSizeType)getAddressASLR(0x2DF960);
		static const getFileNameType getFileName = (getFileNameType)getAddressASLR(0x2DF980);
		const char* fileName = getFileName(section, index);
		if (fileName)
		{
			bool pathFound = false;
			try
			{
				for (auto pathIt = modsPaths.cbegin(); pathIt != modsPaths.cend(); ++pathIt)
				{
					const std::wstring filePath = *pathIt / fileName;
					const std::wstring dirPath = filePath + L".dir";
					if (std::filesystem::exists(dirPath) || std::filesystem::exists(filePath))
					{
						pathFound = true;
						break;
					}
				}
			}
			catch (...) {}
			//do it outside of try just in case
			if (!pathFound && getSize(section, index) == -1)
				return 0;

			return readVBFById(section, index, dest, size, startOffset);
		}
		return 0;
	}

	VBFReader::unpackedFile FileLoader::getBaseFile(const std::filesystem::path& file, std::vector<std::filesystem::path>::const_iterator it, const std::vector<std::filesystem::path>::const_iterator cend, uint32_t blocks)
	{
		VBFReader::unpackedFile ret;
		for (auto pathIt = it; pathIt != cend; ++pathIt)
		{
			const auto filePath = *pathIt / file;
			if (std::filesystem::exists(filePath))
			{
				auto filesize = std::filesystem::file_size(filePath);
				auto readsize = filesize;
				if (blocks > 0)
				{
					if (readsize > 65536ULL * blocks)
						readsize = 65536ULL * blocks;
				}
				if (filesize > 0)
				{
					std::ifstream file(filePath, std::ios::binary);
					ret.data.reset(new uint8_t[readsize]);
					file.read(reinterpret_cast<char*>(ret.data.get()), readsize);
					ret.size = filesize;
					break;
				}
			}
		}
		return ret;
	}

	uintptr_t FileLoader::getAddressASLR(uintptr_t address)
	{
		return address - _defaultModuleBase + _moduleBase;
	}

}
